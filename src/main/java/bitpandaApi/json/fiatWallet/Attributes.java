
package bitpandaApi.json.fiatWallet;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fiat_id",
    "fiat_symbol",
    "balance",
    "name",
    "pending_transactions_count"
})
public class Attributes {

    @JsonProperty("fiat_id")
    private String fiatId;
    @JsonProperty("fiat_symbol")
    private String fiatSymbol;
    @JsonProperty("balance")
    private String balance;
    @JsonProperty("name")
    private String name;
    @JsonProperty("pending_transactions_count")
    private Integer pendingTransactionsCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("fiat_id")
    public String getFiatId() {
        return fiatId;
    }

    @JsonProperty("fiat_id")
    public void setFiatId(String fiatId) {
        this.fiatId = fiatId;
    }

    @JsonProperty("fiat_symbol")
    public String getFiatSymbol() {
        return fiatSymbol;
    }

    @JsonProperty("fiat_symbol")
    public void setFiatSymbol(String fiatSymbol) {
        this.fiatSymbol = fiatSymbol;
    }

    @JsonProperty("balance")
    public String getBalance() {
        return balance;
    }

    @JsonProperty("balance")
    public void setBalance(String balance) {
        this.balance = balance;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("pending_transactions_count")
    public Integer getPendingTransactionsCount() {
        return pendingTransactionsCount;
    }

    @JsonProperty("pending_transactions_count")
    public void setPendingTransactionsCount(Integer pendingTransactionsCount) {
        this.pendingTransactionsCount = pendingTransactionsCount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
