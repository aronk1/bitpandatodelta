package bitpandaApi;

import bitpandaApi.enums.AttributesType;
import bitpandaApi.json.trade.Time;
import csv.delta.enums.Type;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.TimeZone;

public class BitpandaToDelta {

  public static Type convert(AttributesType attributesType) {
    switch (attributesType) {
      case BUY:
        return Type.BUY;
      case DEPOSIT:
        return Type.DEPOSIT;
      case SELL:
        return Type.SELL;
      case TRANSFER:
        return Type.TRANSFER;
      case WITHDRAW:
        return Type.WITHDRAW;
      default:
      case FIAT:
        throw new UnsupportedOperationException();
    }
  }

  public static LocalDateTime convert(Time time) {
    if (time == null || time.getDateIso8601() == null) {
      return null;
    }
    try {
      return LocalDateTime.parse(time.getDateIso8601());
    } catch (Exception e) {
      long timestamp = Long.parseLong(time.getUnix()) * 1000;
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), TimeZone.getDefault().toZoneId());
    }
  }

  public static Double convertNumber(String amountCryptocoin) {
    return Double.parseDouble(amountCryptocoin);
  }

  public static String getCurrencyByFiatId(String fiatId, Map<String, String> fiat) {
    return fiat.get(fiatId);
  }

  public static String getCurrencyByCryptoCoinId(String cryptocoinId, Map<String, String> cryptos) {
    return cryptos.get(cryptocoinId);
  }
}
