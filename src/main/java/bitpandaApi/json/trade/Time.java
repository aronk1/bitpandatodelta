
package bitpandaApi.json.trade;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date_iso8601",
    "unix"
})
public class Time implements Serializable
{

    @JsonProperty("date_iso8601")
    private String dateIso8601;
    @JsonProperty("unix")
    private String unix;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 7291390791601400048L;

    @JsonProperty("date_iso8601")
    public String getDateIso8601() {
        return dateIso8601;
    }

    @JsonProperty("date_iso8601")
    public void setDateIso8601(String dateIso8601) {
        this.dateIso8601 = dateIso8601;
    }

    @JsonProperty("unix")
    public String getUnix() {
        return unix;
    }

    @JsonProperty("unix")
    public void setUnix(String unix) {
        this.unix = unix;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
