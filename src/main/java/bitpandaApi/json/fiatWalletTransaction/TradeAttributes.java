
package bitpandaApi.json.fiatWalletTransaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "type",
    "cryptocoin_id",
    "fiat_id",
    "amount_fiat",
    "amount_cryptocoin",
    "fiat_to_eur_rate",
    "wallet_id",
    "fiat_wallet_id",
    "payment_option_id",
    "time",
    "price",
    "is_swap",
    "is_savings",
    "tags",
    "bfc_used",
    "best_fee_collection"
})
public class TradeAttributes {

    @JsonProperty("status")
    private String status;
    @JsonProperty("type")
    private String type;
    @JsonProperty("cryptocoin_id")
    private String cryptocoinId;
    @JsonProperty("fiat_id")
    private String fiatId;
    @JsonProperty("amount_fiat")
    private String amountFiat;
    @JsonProperty("amount_cryptocoin")
    private String amountCryptocoin;
    @JsonProperty("fiat_to_eur_rate")
    private String fiatToEurRate;
    @JsonProperty("wallet_id")
    private String walletId;
    @JsonProperty("fiat_wallet_id")
    private String fiatWalletId;
    @JsonProperty("payment_option_id")
    private String paymentOptionId;
    @JsonProperty("time")
    private Time time;
    @JsonProperty("price")
    private String price;
    @JsonProperty("is_swap")
    private Boolean isSwap;
    @JsonProperty("is_savings")
    private Boolean isSavings;
    @JsonProperty("tags")
    private List<Object> tags = new ArrayList<Object>();
    @JsonProperty("bfc_used")
    private Boolean bfcUsed;
    @JsonProperty("best_fee_collection")
    private BestFeeCollection bestFeeCollection;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("cryptocoin_id")
    public String getCryptocoinId() {
        return cryptocoinId;
    }

    @JsonProperty("cryptocoin_id")
    public void setCryptocoinId(String cryptocoinId) {
        this.cryptocoinId = cryptocoinId;
    }

    @JsonProperty("fiat_id")
    public String getFiatId() {
        return fiatId;
    }

    @JsonProperty("fiat_id")
    public void setFiatId(String fiatId) {
        this.fiatId = fiatId;
    }

    @JsonProperty("amount_fiat")
    public String getAmountFiat() {
        return amountFiat;
    }

    @JsonProperty("amount_fiat")
    public void setAmountFiat(String amountFiat) {
        this.amountFiat = amountFiat;
    }

    @JsonProperty("amount_cryptocoin")
    public String getAmountCryptocoin() {
        return amountCryptocoin;
    }

    @JsonProperty("amount_cryptocoin")
    public void setAmountCryptocoin(String amountCryptocoin) {
        this.amountCryptocoin = amountCryptocoin;
    }

    @JsonProperty("fiat_to_eur_rate")
    public String getFiatToEurRate() {
        return fiatToEurRate;
    }

    @JsonProperty("fiat_to_eur_rate")
    public void setFiatToEurRate(String fiatToEurRate) {
        this.fiatToEurRate = fiatToEurRate;
    }

    @JsonProperty("wallet_id")
    public String getWalletId() {
        return walletId;
    }

    @JsonProperty("wallet_id")
    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    @JsonProperty("fiat_wallet_id")
    public String getFiatWalletId() {
        return fiatWalletId;
    }

    @JsonProperty("fiat_wallet_id")
    public void setFiatWalletId(String fiatWalletId) {
        this.fiatWalletId = fiatWalletId;
    }

    @JsonProperty("payment_option_id")
    public String getPaymentOptionId() {
        return paymentOptionId;
    }

    @JsonProperty("payment_option_id")
    public void setPaymentOptionId(String paymentOptionId) {
        this.paymentOptionId = paymentOptionId;
    }

    @JsonProperty("time")
    public Time getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Time time) {
        this.time = time;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("is_swap")
    public Boolean getIsSwap() {
        return isSwap;
    }

    @JsonProperty("is_swap")
    public void setIsSwap(Boolean isSwap) {
        this.isSwap = isSwap;
    }

    @JsonProperty("is_savings")
    public Boolean getIsSavings() {
        return isSavings;
    }

    @JsonProperty("is_savings")
    public void setIsSavings(Boolean isSavings) {
        this.isSavings = isSavings;
    }

    @JsonProperty("tags")
    public List<Object> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    @JsonProperty("bfc_used")
    public Boolean getBfcUsed() {
        return bfcUsed;
    }

    @JsonProperty("bfc_used")
    public void setBfcUsed(Boolean bfcUsed) {
        this.bfcUsed = bfcUsed;
    }

    @JsonProperty("best_fee_collection")
    public BestFeeCollection getBestFeeCollection() {
        return bestFeeCollection;
    }

    @JsonProperty("best_fee_collection")
    public void setBestFeeCollection(BestFeeCollection bestFeeCollection) {
        this.bestFeeCollection = bestFeeCollection;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
