
package bitpandaApi.json.assetWallet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "wallets"
})
public class CryptocoinCollectionAttributes {

    @JsonProperty("wallets")
    private List<CryptocoinWallet> cryptocoinWallets = new ArrayList<CryptocoinWallet>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("wallets")
    public List<CryptocoinWallet> getWallets() {
        return cryptocoinWallets;
    }

    @JsonProperty("wallets")
    public void setWallets(List<CryptocoinWallet> cryptocoinWallets) {
        this.cryptocoinWallets = cryptocoinWallets;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
