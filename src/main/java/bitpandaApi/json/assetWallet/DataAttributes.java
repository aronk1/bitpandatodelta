
package bitpandaApi.json.assetWallet;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cryptocoin",
    "commodity",
    "index",
    "security"
})
public class DataAttributes {

    @JsonProperty("cryptocoin")
    private Cryptocoin cryptocoin;
    @JsonProperty("commodity")
    private Commodity commodity;
    @JsonProperty("index")
    private Index index;
    @JsonProperty("security")
    private Security security;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cryptocoin")
    public Cryptocoin getCryptocoin() {
        return cryptocoin;
    }

    @JsonProperty("cryptocoin")
    public void setCryptocoin(Cryptocoin cryptocoin) {
        this.cryptocoin = cryptocoin;
    }

    @JsonProperty("commodity")
    public Commodity getCommodity() {
        return commodity;
    }

    @JsonProperty("commodity")
    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty("index")
    public Index getIndex() {
        return index;
    }

    @JsonProperty("index")
    public void setIndex(Index index) {
        this.index = index;
    }

    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
