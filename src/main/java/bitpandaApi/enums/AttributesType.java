package bitpandaApi.enums;

import java.util.Arrays;

public enum AttributesType {
  BUY("buy"),
  DEPOSIT("deposit"),
  FIAT("Fiat amount (FIAT"),
  SELL("sell"),
  TRANSFER("transfer"),
  WITHDRAW("withdrawl");


  private final String name;

  AttributesType(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public static AttributesType getByName(String name){
    return Arrays.stream(AttributesType.values()).sequential().filter(at -> at.getName().equals(name)).findFirst().orElse(null);
  }
}
