package csv.delta.enums;

public enum Type {
  BUY,
  SELL,
  DEPOSIT,
  WITHDRAW,
  TRANSFER
}
