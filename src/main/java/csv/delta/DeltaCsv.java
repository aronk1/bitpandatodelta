package csv.delta;

import bitpandaApi.enums.AttributesType;
import bitpandaApi.json.trade.Attributes;
import bitpandaApi.json.trade.BestFeeCollection;
import bitpandaApi.json.trade.Data;
import csv.parent.Csv;
import csv.parent.Row;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static bitpandaApi.BitpandaToDelta.*;

public class DeltaCsv<T extends Row> extends Csv<CsvRow> {

  public DeltaCsv(List<Data> data, Map<String, String> crypto, Map<String, String> fiat) {
    super(data, crypto, fiat);
  }

  protected void createRows() {
    Map<String, String> cryptos = getCryptos();
    Map<String, String> fiat = getFiat();
    Set<String> bestFeeTimestamps = new HashSet<>();
    for (Data data : getDataList()) {
      CsvRow row = new CsvRow();
      Attributes attributes = data.getAttributes();

      row.setDate(convert(attributes.getTime()));
      row.setType(convert(AttributesType.getByName(attributes.getType())));
      //row.setExchange("Bitpanda");
      row.setBaseAmount(convertNumber(attributes.getAmountCryptocoin()));
      String baseCurrency = getCurrencyByCryptoCoinId(attributes.getCryptocoinId(), cryptos);
      row.setBaseCurrency(baseCurrency);
      row.setQuoteAmount(convertNumber(attributes.getAmountFiat()));
      row.setQuoteCurrency(getCurrencyByFiatId(attributes.getFiatId(), fiat));
      if (Boolean.TRUE.equals(attributes.getBfcUsed()) && !bestFeeTimestamps.contains(attributes.getTime().getDateIso8601())) {
        row.setFee(getFee(attributes.getBestFeeCollection()));
        row.setFeeCurrency("BEST");
        bestFeeTimestamps.add(attributes.getTime().getDateIso8601());
      }
      row.setSyncHoldings(0);
      if (StringUtils.isBlank(baseCurrency)) {
        System.out.printf("Fehlender CryptoId-Eintrag '%s'. Row: %s%n", attributes.getCryptocoinId(), row.toString());
      }
      getAllRows().add(row);
    }

  }

  private static Double getFee(BestFeeCollection bestFeeCollection) {
    Double fee = null;
    if (bestFeeCollection != null &&
        bestFeeCollection.getAttributes() != null &&
        bestFeeCollection.getAttributes().getWalletTransaction() != null &&
        bestFeeCollection.getAttributes().getWalletTransaction().getAttributes() != null) {
      String feeString = bestFeeCollection.getAttributes().getWalletTransaction().getAttributes().getFee();
      fee = Double.parseDouble(feeString);
    }
    return fee;
  }

  protected List<String> getColumnValues(CsvRow row) {
    return row.getColumnValues(CsvColumn.class);
  }

  public void write(boolean append) {
    createRows();
    saveRowsToCsv(getAllRows(), CsvColumn.getAllColumnNames(), "deltaFile.csv", append);
  }

}
