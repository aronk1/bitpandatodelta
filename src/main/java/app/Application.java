package app;

import bitpandaApi.json.fiatWallet.Fiatwallet;
import bitpandaApi.json.trade.Data;
import bitpandaApi.json.trade.Trade;
import bitpandaApi.json.wallet.Wallet;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csv.cointrackerIo.CointrackerCsv;
import csv.cointrackerIo.CsvRow;
import csv.delta.DeltaCsv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Application {

  private static final String API_BASE_URL = "https://api.bitpanda.com/v1/";
  private static final String TRADES = "trades";
  private static final String WALLETS = "wallets";
  private static final String WALLET_TRANSACTIONS = "wallets/transactions";
  private static final String ASSET_WALLETS = "asset-wallets";
  private static final String ASSET_TRANSACTIONS_COMMODITY = "assets/transactions/commodity";
  private static final String FIAT_WALLETS = "fiatwallets";
  private static final String FIAT_WALLET_TRANSACTIONS = "fiatwallets/transactions";

  private static final String API_KEY_APP_PROPERTY = "apiKey";

  private static String apiKey;

  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("API-Key nicht angegeben.");
      return;
    }
    apiKey = args[0];
    try {
      int page = 1;
      int pageSize = 100;
      Integer totalCount = null;
      int currentCount = 0;
      var cryptos = getCrypto();
      var fiats = getFiat();
      while (totalCount == null || currentCount < totalCount) {
        String response = getResponse(API_BASE_URL + TRADES, page, pageSize);
        Trade answer = new ObjectMapper().readValue(response, Trade.class);
        totalCount = answer.getMeta().getTotalCount();
        List<Data> data = answer.getData();
        currentCount += data.size();

        CointrackerCsv<CsvRow> cointrackerCsv = new CointrackerCsv<>(data, cryptos, fiats);
        cointrackerCsv.write(page != 1);
        DeltaCsv<csv.delta.CsvRow> deltaCsv = new DeltaCsv<>(data, cryptos, fiats);
        deltaCsv.write(page != 1);
        page++;
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static String getResponse(String url, int page, int pageSize) {
    try {
      URL obj = new URL(url + "?page_size=" + pageSize + "&page=" + page);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();
      con.setRequestProperty("X-API-KEY", apiKey);
      con.setRequestProperty("User-Agent", "Mozilla/5.0");
      int responseCode = con.getResponseCode();
      if (responseCode == 200) {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();
        return response.toString();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return "";
  }

  private static Map<String, String> getCrypto() {
    String walletsResponse = getResponse(API_BASE_URL + WALLETS, 1, 1000);
    ObjectMapper objectMapper = new ObjectMapper();
    Map<String, String> cryptos = new HashMap<>();
    cryptos.put("45", "BSV");
    cryptos.put("46", "BNB");
    cryptos.put("49", "XMR");
    cryptos.put("64", "ZIL");
    cryptos.put("65", "THETA");
    try {
      Wallet wallet = objectMapper.readValue(walletsResponse, Wallet.class);
      for (bitpandaApi.json.wallet.Data walletData : wallet.getData()) {
        bitpandaApi.json.wallet.Attributes attributes = walletData.getAttributes();
        cryptos.put(attributes.getCryptocoinId(), attributes.getCryptocoinSymbol());
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    System.out.println(cryptos.toString());
    return cryptos;
  }

  private static Map<String, String> getFiat() {
    String fiatResponse = getResponse(API_BASE_URL + FIAT_WALLETS, 1, 1000);
    ObjectMapper objectMapper = new ObjectMapper();
    Map<String, String> fiats = new HashMap<>();
    try {
      Fiatwallet wallet = objectMapper.readValue(fiatResponse, Fiatwallet.class);
      for (bitpandaApi.json.fiatWallet.Data walletData : wallet.getData()) {
        bitpandaApi.json.fiatWallet.Attributes attributes = walletData.getAttributes();
        fiats.put(attributes.getFiatId(), attributes.getFiatSymbol());
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return fiats;
  }

}
