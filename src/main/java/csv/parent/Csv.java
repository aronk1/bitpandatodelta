package csv.parent;

import bitpandaApi.json.trade.Data;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Csv<T extends Row> {

  private final List<Data> dataList;
  private final Map<String, String> cryptos;
  private final Map<String, String> fiat;
  private final List<T> allRows = new ArrayList<>();

  protected Csv(List<Data> data, Map<String, String> cryptos, Map<String, String> fiat) {
    this.dataList = data;
    this.cryptos = cryptos;
    this.fiat = fiat;
  }

  protected abstract void write(boolean append);

  protected abstract void createRows();

  public List<Data> getDataList() {
    return dataList;
  }

  public Map<String, String> getCryptos() {
    return cryptos;
  }

  public Map<String, String> getFiat() {
    return fiat;
  }

  public List<T> getAllRows() {
    return allRows;
  }

  protected void saveRowsToCsv(List<T> allRows, String[] allColumnNames, String filename, boolean append) {
    try (FileWriter fw = new FileWriter(filename, append); StringWriter stringWriter = new StringWriter()) {
      CSVPrinter csvPrinter = new CSVPrinter(stringWriter, append ? CSVFormat.DEFAULT : CSVFormat.DEFAULT.withHeader(allColumnNames));
      for (T row : allRows) {
        csvPrinter.printRecord(getColumnValues(row));
      }
      csvPrinter.flush();
      fw.append(stringWriter.toString());
      fw.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  protected abstract List<String> getColumnValues(T row);
}
