
package bitpandaApi.json.fiatWalletTransaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fiat_wallet_id",
    "user_id",
    "fiat_id",
    "amount",
    "fee",
    "to_eur_rate",
    "time",
    "in_or_out",
    "type",
    "status",
    "confirmation_by",
    "confirmed",
    "payment_option_id",
    "requires_2fa_approval",
    "is_savings",
    "last_changed",
    "tags",
    "public_status",
    "bank_account_details",
    "is_index",
    "is_card",
    "trade"
})
public class FiatWalletTransactionAttributes {

    @JsonProperty("fiat_wallet_id")
    private String fiatWalletId;
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("fiat_id")
    private String fiatId;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("fee")
    private String fee;
    @JsonProperty("to_eur_rate")
    private String toEurRate;
    @JsonProperty("time")
    private Time time;
    @JsonProperty("in_or_out")
    private String inOrOut;
    @JsonProperty("type")
    private String type;
    @JsonProperty("status")
    private String status;
    @JsonProperty("confirmation_by")
    private String confirmationBy;
    @JsonProperty("confirmed")
    private Boolean confirmed;
    @JsonProperty("payment_option_id")
    private String paymentOptionId;
    @JsonProperty("requires_2fa_approval")
    private Boolean requires2faApproval;
    @JsonProperty("is_savings")
    private Boolean isSavings;
    @JsonProperty("last_changed")
    private LastChanged lastChanged;
    @JsonProperty("tags")
    private List<Object> tags = new ArrayList<Object>();
    @JsonProperty("public_status")
    private String publicStatus;
    @JsonProperty("bank_account_details")
    private BankAccountDetails bankAccountDetails;
    @JsonProperty("is_index")
    private Boolean isIndex;
    @JsonProperty("is_card")
    private Boolean isCard;
    @JsonProperty("trade")
    private Trade trade;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("fiat_wallet_id")
    public String getFiatWalletId() {
        return fiatWalletId;
    }

    @JsonProperty("fiat_wallet_id")
    public void setFiatWalletId(String fiatWalletId) {
        this.fiatWalletId = fiatWalletId;
    }

    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("fiat_id")
    public String getFiatId() {
        return fiatId;
    }

    @JsonProperty("fiat_id")
    public void setFiatId(String fiatId) {
        this.fiatId = fiatId;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("fee")
    public String getFee() {
        return fee;
    }

    @JsonProperty("fee")
    public void setFee(String fee) {
        this.fee = fee;
    }

    @JsonProperty("to_eur_rate")
    public String getToEurRate() {
        return toEurRate;
    }

    @JsonProperty("to_eur_rate")
    public void setToEurRate(String toEurRate) {
        this.toEurRate = toEurRate;
    }

    @JsonProperty("time")
    public Time getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Time time) {
        this.time = time;
    }

    @JsonProperty("in_or_out")
    public String getInOrOut() {
        return inOrOut;
    }

    @JsonProperty("in_or_out")
    public void setInOrOut(String inOrOut) {
        this.inOrOut = inOrOut;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("confirmation_by")
    public String getConfirmationBy() {
        return confirmationBy;
    }

    @JsonProperty("confirmation_by")
    public void setConfirmationBy(String confirmationBy) {
        this.confirmationBy = confirmationBy;
    }

    @JsonProperty("confirmed")
    public Boolean getConfirmed() {
        return confirmed;
    }

    @JsonProperty("confirmed")
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    @JsonProperty("payment_option_id")
    public String getPaymentOptionId() {
        return paymentOptionId;
    }

    @JsonProperty("payment_option_id")
    public void setPaymentOptionId(String paymentOptionId) {
        this.paymentOptionId = paymentOptionId;
    }

    @JsonProperty("requires_2fa_approval")
    public Boolean getRequires2faApproval() {
        return requires2faApproval;
    }

    @JsonProperty("requires_2fa_approval")
    public void setRequires2faApproval(Boolean requires2faApproval) {
        this.requires2faApproval = requires2faApproval;
    }

    @JsonProperty("is_savings")
    public Boolean getIsSavings() {
        return isSavings;
    }

    @JsonProperty("is_savings")
    public void setIsSavings(Boolean isSavings) {
        this.isSavings = isSavings;
    }

    @JsonProperty("last_changed")
    public LastChanged getLastChanged() {
        return lastChanged;
    }

    @JsonProperty("last_changed")
    public void setLastChanged(LastChanged lastChanged) {
        this.lastChanged = lastChanged;
    }

    @JsonProperty("tags")
    public List<Object> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    @JsonProperty("public_status")
    public String getPublicStatus() {
        return publicStatus;
    }

    @JsonProperty("public_status")
    public void setPublicStatus(String publicStatus) {
        this.publicStatus = publicStatus;
    }

    @JsonProperty("bank_account_details")
    public BankAccountDetails getBankAccountDetails() {
        return bankAccountDetails;
    }

    @JsonProperty("bank_account_details")
    public void setBankAccountDetails(BankAccountDetails bankAccountDetails) {
        this.bankAccountDetails = bankAccountDetails;
    }

    @JsonProperty("is_index")
    public Boolean getIsIndex() {
        return isIndex;
    }

    @JsonProperty("is_index")
    public void setIsIndex(Boolean isIndex) {
        this.isIndex = isIndex;
    }

    @JsonProperty("is_card")
    public Boolean getIsCard() {
        return isCard;
    }

    @JsonProperty("is_card")
    public void setIsCard(Boolean isCard) {
        this.isCard = isCard;
    }

    @JsonProperty("trade")
    public Trade getTrade() {
        return trade;
    }

    @JsonProperty("trade")
    public void setTrade(Trade trade) {
        this.trade = trade;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
