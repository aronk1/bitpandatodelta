package csv.delta;

import csv.delta.enums.Type;
import csv.parent.Row;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static csv.delta.CsvColumn.*;

public class CsvRow extends Row<CsvColumn> {

  public void setDate(LocalDateTime date) {
    getRowEntries().put(DATE, date);
  }

  public void setType(Type type) {
    getRowEntries().put(TYPE, type);
  }

  public void setExchange(String exchange) {
    getRowEntries().put(EXCHANGE, exchange);
  }

  public void setBaseAmount(Double baseAmount) {
    getRowEntries().put(BASE_AMOUNT, baseAmount);
  }

  public void setBaseCurrency(String baseCurrency) {
    getRowEntries().put(BASE_CURRENCY, baseCurrency);
  }

  public void setQuoteAmount(Double quoteAmount) {
    getRowEntries().put(QUOTE_AMOUNT, quoteAmount);
  }

  public void setQuoteCurrency(String quoteCurrency) {
    getRowEntries().put(QUOTE_CURRENCY, quoteCurrency);
  }

  public void setFee(Double fee) {
    getRowEntries().put(FEE, fee);
  }

  public void setFeeCurrency(String feeCurrency) {
    getRowEntries().put(FEE_CURRENCY, feeCurrency);
  }

  public void setCostsProceeds(Double costsProceeds) {
    getRowEntries().put(COSTS_PROCEEDS, costsProceeds);
  }

  public void setCostsProceedsCurrency(String costsProceedsCurrency) {
    getRowEntries().put(COSTS_PROCEEDS_CURRENCY, costsProceedsCurrency);
  }

  public void setSyncHoldings(Integer syncHoldings) {
    getRowEntries().put(SYNC_HOLDINGS, syncHoldings);
  }

  public void setSendReceivedFrom(String sendReceivedFrom) {
    getRowEntries().put(SENT_RECEIVED_FROM, sendReceivedFrom);
  }

  public void setSendTo(String sendTo) {
    getRowEntries().put(SENT_TO, sendTo);
  }

  public void setNotes(String notes) {
    getRowEntries().put(NOTES, notes);
  }

  @Override
  protected String formatLocalDateTime(LocalDateTime localDateTime) {
    ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
    String formattedDateTime = zonedDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    formattedDateTime = formattedDateTime.replace("T", " ");
    formattedDateTime = formattedDateTime.replace("+", " +");
    return formattedDateTime;
  }
}
