package csv.cointrackerIo;

import bitpandaApi.enums.AttributesType;
import bitpandaApi.json.trade.Attributes;
import bitpandaApi.json.trade.Data;
import csv.parent.Csv;
import csv.parent.Row;

import java.util.List;
import java.util.Map;

import static bitpandaApi.BitpandaToDelta.*;

public class CointrackerCsv<T extends Row> extends Csv<CsvRow> {

  public CointrackerCsv(List<Data> data, Map<String, String> crypto, Map<String, String> fiat) {
    super(data, crypto, fiat);
  }

  public void createRows() {
    for (Data data : getDataList()) {
      csv.cointrackerIo.CsvRow row = new csv.cointrackerIo.CsvRow();
      Attributes attributes = data.getAttributes();
      boolean isBuy = AttributesType.getByName(attributes.getType()).equals(AttributesType.BUY);

      row.setDate(convert(attributes.getTime()));
      if (isBuy) {
        row.setReceivedQuantity(convertNumber(attributes.getAmountCryptocoin()));
        row.setReceivedCurrency(getCurrencyByCryptoCoinId(attributes.getCryptocoinId(), getCryptos()));
        row.setSentQuantity(convertNumber(attributes.getAmountFiat()));
        row.setSentCurrency(getCurrencyByFiatId(attributes.getFiatId(), getFiat()));
      } else {
        row.setReceivedQuantity(convertNumber(attributes.getAmountFiat()));
        row.setReceivedCurrency(getCurrencyByFiatId(attributes.getFiatId(), getFiat()));
        row.setSentQuantity(convertNumber(attributes.getAmountCryptocoin()));
        row.setSentCurrency(getCurrencyByCryptoCoinId(attributes.getCryptocoinId(), getCryptos()));
      }
      getAllRows().add(row);
    }
  }

  @Override
  protected List<String> getColumnValues(CsvRow row) {
    return row.getColumnValues(CsvColumn.class);
  }

  //  private static void saveRowsToCointrackerCsv(List<csv.cointrackerIo.CsvRow> allRows) {
  //    try (FileWriter fw = new FileWriter("cointrackerIoFile.csv", true); StringWriter stringWriter = new StringWriter()) {
  //      CSVPrinter csvPrinter = new CSVPrinter(stringWriter, CSVFormat.DEFAULT.withHeader(csv.cointrackerIo.CsvColumn.getAllColumnNames()));
  //      for (csv.cointrackerIo.CsvRow row : allRows) {
  //        csvPrinter.printRecord(row.getColumnValues(csv.cointrackerIo.CsvColumn.class));
  //      }
  //      csvPrinter.flush();
  //      fw.append(stringWriter.toString());
  //      fw.flush();
  //    } catch (IOException e) {
  //      e.printStackTrace();
  //    }
  //  }

  public void write(boolean append) {
    createRows();
    saveRowsToCsv(getAllRows(), CsvColumn.getAllColumnNames(), "cointrackerIoFile.csv", append);
  }
}
