package csv.cointrackerIo;

import csv.parent.Row;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static csv.cointrackerIo.CsvColumn.*;

public class CsvRow extends Row<CsvColumn> {

  @Override
  protected String formatLocalDateTime(LocalDateTime localDateTime) {
    String formattedDateTime = localDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    formattedDateTime = formattedDateTime.replace("T", " ");
    formattedDateTime = formattedDateTime.split("\\+")[0];
    return formattedDateTime;
  }

  public void setDate(LocalDateTime date) {
    getRowEntries().put(CsvColumn.DATE, date);
  }

  public void setReceivedQuantity(Double receivedQuantity) {
    getRowEntries().put(RECEIVED_QUANTITY, receivedQuantity);
  }

  public void setReceivedCurrency(String receivedCurrency) {
    getRowEntries().put(RECEIVED_CURRENCY, receivedCurrency);
  }

  public void setSentQuantity(Double sentQuantity) {
    getRowEntries().put(SENT_QUANTITY, sentQuantity);
  }

  public void setSentCurrency(String sentCurrency) {
    getRowEntries().put(SENT_CURRENCY, sentCurrency);
  }

  public void setFeeAmount(Double feeAmount) {
    getRowEntries().put(FEE_AMOUNT, feeAmount);
  }

  public void setFeeCurrency(String feeCurrency) {
    getRowEntries().put(FEE_CURRENCY, feeCurrency);
  }

  public void setTag(String tag) {
    getRowEntries().put(TAG, tag);
  }
}
