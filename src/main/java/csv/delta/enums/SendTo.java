package csv.delta.enums;

public enum SendTo {
  MY_WALLET,
  OTHER_WALLET,
  BANK,
  OTHER
}
