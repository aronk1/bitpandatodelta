
package bitpandaApi.json.fiatWalletTransaction;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "best_current_price_eur",
    "best_used_price_eur",
    "bfc_deduction",
    "bfc_market_value_eur",
    "wallet_transaction"
})
public class BestFeeCollectionAttributes {

    @JsonProperty("best_current_price_eur")
    private String bestCurrentPriceEur;
    @JsonProperty("best_used_price_eur")
    private String bestUsedPriceEur;
    @JsonProperty("bfc_deduction")
    private Double bfcDeduction;
    @JsonProperty("bfc_market_value_eur")
    private String bfcMarketValueEur;
    @JsonProperty("wallet_transaction")
    private WalletTransaction walletTransaction;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("best_current_price_eur")
    public String getBestCurrentPriceEur() {
        return bestCurrentPriceEur;
    }

    @JsonProperty("best_current_price_eur")
    public void setBestCurrentPriceEur(String bestCurrentPriceEur) {
        this.bestCurrentPriceEur = bestCurrentPriceEur;
    }

    @JsonProperty("best_used_price_eur")
    public String getBestUsedPriceEur() {
        return bestUsedPriceEur;
    }

    @JsonProperty("best_used_price_eur")
    public void setBestUsedPriceEur(String bestUsedPriceEur) {
        this.bestUsedPriceEur = bestUsedPriceEur;
    }

    @JsonProperty("bfc_deduction")
    public Double getBfcDeduction() {
        return bfcDeduction;
    }

    @JsonProperty("bfc_deduction")
    public void setBfcDeduction(Double bfcDeduction) {
        this.bfcDeduction = bfcDeduction;
    }

    @JsonProperty("bfc_market_value_eur")
    public String getBfcMarketValueEur() {
        return bfcMarketValueEur;
    }

    @JsonProperty("bfc_market_value_eur")
    public void setBfcMarketValueEur(String bfcMarketValueEur) {
        this.bfcMarketValueEur = bfcMarketValueEur;
    }

    @JsonProperty("wallet_transaction")
    public WalletTransaction getWalletTransaction() {
        return walletTransaction;
    }

    @JsonProperty("wallet_transaction")
    public void setWalletTransaction(WalletTransaction walletTransaction) {
        this.walletTransaction = walletTransaction;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
